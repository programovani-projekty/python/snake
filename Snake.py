import pygame
from pygame.locals import *
import time
import random

pygame.init()

# PROGRAM INICIACE

# colors
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 100, 0)

# display size
dis_width = 800
dis_height = 600
dis = pygame.display.set_mode((dis_width, dis_height), RESIZABLE)
pygame.display.set_caption("Snake by Pavel Pikola")

want_end = False


# definition of my functions
def message(message, color):
    pp = font_style.render(message, True, color)
    width_text = pp.get_width()
    height_text = pp.get_height()
    dis.blit(pp, [(dis_width - width_text)/2,
                  (dis_height - height_text)/2 - 120])


def drawing(x, y, color, text):
    ii = font_style.render(text, True, color)
    dis.blit(ii, [x, y])


# GAME INICIATION
while not want_end:

    # game loop variable
    game_over = False

    # iniciation of score
    score = 0

    # iniciation of fonts
    font_style = pygame.font.SysFont(None, 50)

    # INICIATION OF SNAKE
    # x1, y1 - starting position of snake
    x1 = dis_width/2
    y1 = dis_height/2
    snake_block = 20  # size of snake head in pixels
    clock = pygame.time.Clock()
    snake_speed = 10

    # body of snake
    body = []

    # starting direction of snake
    x1_direction = 0
    y1_direction = 0

    # INICIATION OF FOOD
    x1_position = snake_block + snake_block * \
        random.randint(0, (dis_width-snake_block*2)/snake_block-1)
    y1_position = snake_block + snake_block * \
        random.randint(0, (dis_height-snake_block*2)/snake_block-1)

    # Starting game loop
    while not game_over:

        # loading events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
                want_end = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x1_direction = -snake_block
                    y1_direction = 0
                elif event.key == pygame.K_RIGHT:
                    x1_direction = snake_block
                    y1_direction = 0
                elif event.key == pygame.K_UP:
                    y1_direction = -snake_block
                    x1_direction = 0
                elif event.key == pygame.K_DOWN:
                    y1_direction = snake_block
                    x1_direction = 0

        # snake movement
        x1 += x1_direction
        y1 += y1_direction

        head = []
        head.append(x1)
        head.append(y1)

        # snake head collision check
        try:
            i = body.index(head, 0, len(body)-1)
            game_over = True
        except ValueError:
            pass

        # adding head to body
        body.append(head)

    # end the game when the snake hits the borders
        if (x1 >= dis_width - snake_block*2) or (y1 >= dis_height - snake_block*2) or (x1 <= snake_block) or (y1 <= snake_block):

            game_over = True

        # when snake eat food
        if (x1 == x1_position) and (y1 == y1_position):
            # calculation of a new random position of food
            x1_position = snake_block + snake_block * \
                random.randint(0, (dis_width-snake_block*2)/snake_block-1)
            y1_position = snake_block + snake_block * \
                random.randint(0, (dis_height-snake_block*2)/snake_block-1)
            score += 1
        else:
            if len(body) > 1:
                body.pop(0)

        # STARTING DRAWING
        # fills the entire display in red
        pygame.draw.rect(dis, red, [0, 0, 800, 600])
        pygame.draw.rect(
            dis, white, [snake_block, snake_block, 800-2*snake_block, 600-2*snake_block])

        # fills the entire display in white
        pygame.draw.rect(
            dis, green, [x1_position, y1_position, snake_block, snake_block])  # drawing to the desktop

        # drawing snake
        for item in body:
            pygame.draw.rect(
                dis, black, [item[0], item[1], snake_block, snake_block])

        # drawing score
        drawing(25, 25, black, "Score: " + str(score))
        # STOP DRAWING

        pygame.display.update()
        clock.tick(snake_speed)
    # KONEC SMYCKY HRY

    if not want_end:
        message("You lose, Try again!", red)

        # drawing rectangle
        button_x = (dis_width-200)/2
        button_y = ((dis_height-70)/2) - 30
        pygame.draw.rect(
            dis, red, [button_x, button_y, 200, 70])

        # write in the rectangle "Play again"
        drawing(button_x + 10, button_y + 20, white, "Play again!")

        pygame.display.update()

        reaction = False

        while (not reaction) and (not want_end):
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP:
                    mx = event.pos[0]
                    my = event.pos[1]
                    if (mx > button_x) and (mx < button_x + 200) and (my > button_y) and (my < button_y + 70):
                        reaction = True
                        want_end = False
                if event.type == pygame.MOUSEMOTION:
                    mx = event.pos[0]
                    my = event.pos[1]
                    if (mx > button_x) and (mx < button_x + 200) and (my > button_y) and (my < button_y + 70):
                        drawing(button_x + 10, button_y +
                                20, black, "Play again!")
                    else:
                        drawing(button_x + 10, button_y +
                                20, white, "Play again!")
                    pygame.display.update()

                if event.type == pygame.QUIT:
                    want_end = True


# END OF MAIN LOOP

pygame.quit()

quit()
